FROM openjdk:8
LABEL org.opencontainers.image.authors="azolotuhin96@gmail.com"
LABEL fio="Zolotukhin Alexey Vladimirovich"
WORKDIR app
COPY ["Hello.java","WORKDIR"]
RUN ["javac","Hello.java"]
CMD ["java","Hello.java"]